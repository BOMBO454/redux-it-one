import React, {useEffect} from 'react';
import './App.css';
import {BrowserRouter, NavLink, Route, Routes} from "react-router-dom";
import {LoginPage} from "./pages/LoginPage";
import {useCurrentUser} from "./hooks";
import {Menu} from "semantic-ui-react";
import {CabinetPage} from "./pages/CabinetPage";
import {TodoPage} from "./pages/TodoPage";
import {useDispatch} from "react-redux";
import {getTodo} from "./api/todos";
import {createTodosAction} from "./store/todo/todoActions";

function App() {
  const user = useCurrentUser()
  return (
    <BrowserRouter>
      <Menu>
        <Menu.Item>
          <NavLink to={"/"}>{user?user.name:"Войти"}</NavLink>
        </Menu.Item>
        {user && <>
        <Menu.Item>
          <NavLink to={"/to-do"}>ТуДу</NavLink>
        </Menu.Item>
        <Menu.Item>
          <NavLink to={"/cabinet"}>Кабинет</NavLink>
        </Menu.Item>
        </>}
      </Menu>
      <Routes>
        <Route
          path="/"
          element={<LoginPage/>}
        />
        <Route
          path="/cabinet"
          element={<CabinetPage/>}
        />
        <Route
          path="/to-do"
          element={<TodoPage/>}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
