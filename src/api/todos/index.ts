export const getTodo = async () => {
  return await fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json())
    .then((json) => json);
};

export const addTodo = async ({
  title,
  userId,
  completed,
}: {
  title: string;
  userId: number;
  completed: boolean;
}) => {
  return await fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
    body: JSON.stringify({
      title: title,
      userId: userId,
      completed: completed,
    }),
  })
    .then((response) => response.json())
    .then((json) => json);
};
