import {
  ADD_TODOS_LOADING_TYPE,
  ADD_TODOS_SUCCESS_TYPE,
  CREATE_TODOS_TYPE,
  TodoActions,
} from './todoActions';
import { Todo } from '../../type';

export interface TodoState {
  todos: Array<Todo>;
  loading: boolean;
}

const initialState: TodoState = {
  todos: [],
  loading: false,
};

export function todoReducer(state = initialState, action: TodoActions) {
  switch (action.type) {
    case CREATE_TODOS_TYPE:
      return { ...state, todos: action.payload.todos };
    case ADD_TODOS_LOADING_TYPE:
      return { ...state, loading: action.payload.status };
    case ADD_TODOS_SUCCESS_TYPE:
      return { loading: false, todos: [...state.todos, action.payload.todo] };
    default:
      return state;
  }
}
