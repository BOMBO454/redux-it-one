import { RootState, Todo } from '../../type';

export const todoSelector = ({ todo }: RootState): Todo[] => {
  return todo.todos;
};
