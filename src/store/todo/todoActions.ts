import {RootState, Todo} from '../../type';
import {addTodo} from '../../api/todos';
import {Action, Dispatch} from 'redux';
import {ThunkAction} from "redux-thunk";
import {TodoState} from "./todoReducer";

export const CREATE_TODOS_TYPE = 'CREATE_USER_TYPE';
export const ADD_TODOS_LOADING_TYPE = 'ADD_TODOS_LOADING_TYPE';
export const ADD_TODOS_SUCCESS_TYPE = 'ADD_TODOS_LOADING_TYPE';

export const startedLoadingAction = ({
                                       status,
                                     }: {
  status: boolean;
}): { type: typeof ADD_TODOS_LOADING_TYPE; payload: { status: boolean } } => {
  return {
    type: ADD_TODOS_LOADING_TYPE,
    payload: {status: status},
  };
};

export const finishLoadingAction = ({
                                      todo,
                                    }: {
  todo: Todo;
}): { type: typeof ADD_TODOS_SUCCESS_TYPE; payload: { todo: Todo } } => {
  return {
    type: ADD_TODOS_SUCCESS_TYPE,
    payload: {todo: todo},
  };
};

export const createTodosAction = (
  todos: Array<Todo>,
): {
  type: string;
  payload: { todos: Todo[] };
} => {
  return {
    type: CREATE_TODOS_TYPE,
    payload: {todos: todos},
  };
};

export const addTodoThunk = ({
                                title,
                                userId,
                                completed,
                              }: {
  title: string;
  userId: number;
  completed: boolean;
}): ThunkAction<void, RootState, void, Action<string>> => {
  return (dispatch: Dispatch) => {
    dispatch(startedLoadingAction({status: true}));
    addTodo({title, userId, completed})
      .then((data) => {
        finishLoadingAction({todo: data});
      })
      .catch(() => {
        dispatch(startedLoadingAction({status: false}));
      });
  };
};

export type TodoActions =
  | ReturnType<typeof createTodosAction>
  | ReturnType<typeof startedLoadingAction>
  | ReturnType<typeof finishLoadingAction>;
