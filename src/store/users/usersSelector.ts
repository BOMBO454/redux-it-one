import { RootState } from '../../type';

export const usersSelector = ({ users }: RootState) => {
  return users.users;
};
