import {User} from "../../type";

export const USERS_PULL = "USERS_PULL"

export interface UsersPullProps {
  users: Array<User>
}

export function usersPull({users}: UsersPullProps): { type: typeof USERS_PULL, payload: { users: Array<User> } } {
  return {
    type: USERS_PULL,
    payload: {users: users}
  };
}

export type UsersActions = ReturnType<typeof usersPull>
