import {
  USERS_PULL,
  UsersActions, UsersPullProps,
} from './usersActions';
import {User} from '../../type';

export interface UsersState {
  users: Array<User>;
}

const initialState: UsersState = {
  users: [
    {
      id: 0,
      name: 'admin',
      secondName: 'adminov',
      age: 99,
      password: 'admin',
    },
  ],
};

const userPull = (state: UsersState, {users}: UsersPullProps):UsersState => {
  return {users: users}
}

export function usersReducer(state = initialState, action: UsersActions) {
  switch (action.type) {
    case USERS_PULL:
      return userPull(state, action.payload)
    default:
      return state;
  }
}
