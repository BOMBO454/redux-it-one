import {combineReducers} from "redux";
import {userReducer} from "./user/userReducer";
import {todoReducer} from "./todo/todoReducer";
import {usersReducer} from "./users/userReducer";

export const rootReducer = combineReducers({
  user: userReducer,
  todo: todoReducer,
  users: usersReducer,
})