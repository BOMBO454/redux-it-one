import { RootState } from '../../type';

export const userSelector = ({ user, users }: RootState) => {
  return users.users.find((item) => item.id === user.currentUser);
};
