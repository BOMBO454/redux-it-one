import {LoginUserActionPayload, UserActions, SET_CURRENT_USER,} from './userActions';
import {Reducer} from "redux";

export interface UserState {
  currentUser: number | null;
}

const initialState: UserState = {
  currentUser: null,
};

const userLogin = (state: UserState, {userId}: LoginUserActionPayload): UserState => {
  return {currentUser: userId};
}

export const userReducer: Reducer<UserState, UserActions> = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return userLogin(state, action.payload)
    default:
      return state;
  }
}
