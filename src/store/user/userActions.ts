import {User} from "../../type";

export const SET_CURRENT_USER = "SET_CURRENT_USER"

export interface LoginUserActionPayload {
  userId: User["id"]
}

export function LoginUserAction({userId}: LoginUserActionPayload): { type: typeof SET_CURRENT_USER, payload: { userId: User["id"] } } {
  return {
    type: SET_CURRENT_USER,
    payload: {userId: userId}
  };
}

export type UserActions = ReturnType<typeof LoginUserAction>
