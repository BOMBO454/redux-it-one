import {useEffect, useState} from "react";
import { useSelector} from "react-redux";
import {userSelector} from "../store/user/userSelector";
import {todoSelector} from "../store/todo/todoSelector";
import {User} from "../type";

export const useCurrentUser = () => {
  const currentUser = useSelector(userSelector)
  const [user,setUser] = useState<User|undefined>(currentUser)
  useEffect(()=>{
    setUser(currentUser)
  },[currentUser])
  return user
}

export const useTodo = (from=0, count = 15)=>{
  const todo = useSelector(todoSelector)
  return {todos: todo.slice(from,from + count), total: todo.length}
}