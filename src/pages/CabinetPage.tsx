import React, { ChangeEvent, EventHandler, FC, useEffect, useState } from 'react';
import { Button, Form, Input, Label, Segment } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import { ChangeUserAction, LoginUserAction } from '../store/user/userActions';
import { useCurrentUser } from '../hooks';
import { useNavigate } from 'react-router-dom';

type Props = {};
export const CabinetPage: FC<Props> = (props) => {
  const user = useCurrentUser();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [userData, setUserData] = useState({
    name: user?.name,
    secondName: user?.secondName,
    age: user?.age,
  });
  useEffect(() => {
    if (!user) {
      navigate('/');
    }
  }, [user]);
  const firstNameHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData((data) => ({ ...data, name: e.target.value }));
  };
  const secondNameHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData((data) => ({ ...data, secondName: e.target.value }));
  };
  const ageHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData((data) => ({ ...data, age: e.target.valueAsNumber }));
  };
  const submitHandler = () => {
    if (
      user &&
      userData.name !== undefined &&
      userData.secondName !== undefined &&
      userData.age !== undefined
    ) {
      dispatch(
        ChangeUserAction({
          userIndex: user.id,
          firstName: userData.name,
          secondName: userData.secondName,
          age: userData.age,
        }),
      );
    } else {
      alert('Что то не так');
    }
  };
  return (
    <Segment>
      <Form>
        <Form.Field>
          <label>Кабинет</label>
        </Form.Field>
        <Form.Field>
          <Label>Имя</Label>
          <Input placeholder="Login" value={userData.name} onChange={firstNameHandler}></Input>
        </Form.Field>
        <Form.Field>
          <Label>Фамилия</Label>
          <Input value={userData.secondName} onChange={secondNameHandler}></Input>
        </Form.Field>
        <Form.Field>
          <Label>Возраст</Label>
          <Input type={'number'} value={userData.age} onChange={ageHandler}></Input>
        </Form.Field>
        <Form.Field>
          <Button type="submit" onClick={submitHandler}>
            Подтвердить
          </Button>
        </Form.Field>
      </Form>
    </Segment>
  );
};
