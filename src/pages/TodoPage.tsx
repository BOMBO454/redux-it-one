import React, {ChangeEvent, FC, SyntheticEvent, useEffect, useState} from 'react';
import {Button, Checkbox, Form, Input, Label, Pagination, PaginationProps, Segment} from "semantic-ui-react";
import {useDispatch} from "react-redux";
import {useCurrentUser, useTodo} from "../hooks";
import {useNavigate} from "react-router-dom";
import {getTodo} from "../api/todos";
import {createTodosAction} from "../store/todo/todoActions";

type Props = {}
export const TodoPage: FC<Props> = (props) => {
  const [activePage, setActivePage] = useState<number>(1);
  const {todos, total} = useTodo((activePage-1)*15);
  const pageHandler = (data: PaginationProps) =>{
    setActivePage(Number(data.activePage))
  }
  const dispatch = useDispatch()
  useEffect(()=>{
    getTodo().then((data)=>{
      dispatch(createTodosAction(data))
    })
  },[])
  return (
    <Segment>
      <Form>
        <Form.Field>
          <label>TODO</label>
        </Form.Field>
        {todos.map(todo=>(
          <Form.Field key={todo.id}>
            <Checkbox label={todo.title} checked={todo.completed} />
          </Form.Field>
        ))}
      </Form>
      <Pagination
        activePage={activePage}
        onPageChange={(_,data)=> {
            pageHandler(data)
        }}
        boundaryRange={0}
        defaultActivePage={1}
        siblingRange={1}
        totalPages={Math.ceil(total/15)}
      />
    </Segment>
  )
};
