import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { Button, Form, Input, Label, Segment } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import { LoginUserAction } from '../store/user/userActions';
import { useCurrentUser } from '../hooks';
import { useNavigate } from 'react-router-dom';

type Props = {};
export const LoginPage: FC<Props> = (props) => {
  const dispatch = useDispatch();
  const user = useCurrentUser();
  const navigate = useNavigate();
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (user) {
      navigate('/cabinet');
    }
  }, [user]);

  const loginHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setLogin(e.target.value);
  };
  const passwordHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };
  const submitHandler = () => {
    dispatch(LoginUserAction({ userName: login, userPassword: password }));
  };
  return (
    <Segment>
      <Form>
        <Form.Field>
          <label>Вход</label>
        </Form.Field>
        <Form.Field>
          <Label>Login</Label>
          <Input placeholder="Login" value={login} onChange={loginHandler}></Input>
        </Form.Field>
        <Form.Field>
          <Label>Password</Label>
          <Input
            type={'password'}
            placeholder="Password"
            value={password}
            onChange={passwordHandler}
          ></Input>
        </Form.Field>
        <Button type="submit" onClick={submitHandler}>
          Submit
        </Button>
      </Form>
    </Segment>
  );
};
