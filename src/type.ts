import { UserActions } from './store/user/userActions';
import { TodoActions } from './store/todo/todoActions';
import store from "./store/store";

export type User = {
  id: number;
  name: string;
  password: string;
  secondName: string;
  age: number;
};

export type Todo = {
  completed: boolean;
  id: number;
  title: string;
  userId: number;
};

export type Actions = UserActions | TodoActions;
export type RootState = ReturnType<typeof store.getState>;
